﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace ColorMixer
{
    /// <summary>
    /// Implements the functionality common to all MixFunctions
    /// </summary>
    public abstract class MixFunctionBase:IMixFunction
    {
        /// <summary>
        /// The minimum amount of sources the function needs to mix the final color. For instance, a negation needs just one, but average works best with 2 or more.
        /// </summary>
        public abstract int MinimumSources { get; }
        /// <summary>
        /// Gets the human-readable description of this function
        /// </summary>
        public abstract string Description { get; }
        /// <summary>
        /// Gets the name of the mixing function
        /// </summary>
        public abstract string Name {get; }
        /// <summary>
        /// Mixes colors from the color sources
        /// </summary>
        /// <param name="sources"></param>
        /// <returns></returns>
        public abstract Color Mix(List<IMixer> sources);

        /// <summary>
        /// Determines whether this instance and another specified have the same value
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IMixFunction other)
        {
            return Name.Equals(other.Name);
        }

        /// <summary>
        /// Determines whether this instance and another specified have the same value
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
 	        var other = obj as IMixFunction;
            if (other == null)
                return false;

            return Equals(other);
        }

        /// <summary>
        /// Returns the has code for this instance
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override int GetHashCode()
        {
 	         return Name.GetHashCode();
        }
    }
}
