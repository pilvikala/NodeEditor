﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;

namespace ColorMixer
{
    /// <summary>
    /// Handles creation of all mix functions
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// singleton instance
        /// </summary>
        private static readonly Factory instance = new Factory();

        /// <summary>
        /// Global transparency setting
        /// </summary>
        public static readonly byte GlobalAlpha = 255;

        /// <summary>
        /// Gets the singleton instance
        /// </summary>
        public static Factory Instance { get { return instance; } }

        [ImportMany]
        readonly List<IMixFunction> mixFunctions = new List<IMixFunction>();
        private CompositionContainer container;
        private IMixFunction defaultFunction = new Empty();
        
        /// <summary>
        /// Default empty function doing nothing.
        /// </summary>
        public IMixFunction DefaultFunction { get { return defaultFunction; } }
        /// <summary>
        /// Gets the collection of available Mixers
        /// </summary>
        public List<IMixFunction> MixFunctions { get { return mixFunctions; } }

        /// <summary>
        /// Initializes a new instance of this class
        /// </summary>
        /// <remarks>
        /// This fills in the Mixers collection
        /// </remarks>
        private Factory()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)));
            container = new CompositionContainer(catalog);
            container.ComposeParts(this);
            mixFunctions.Add(defaultFunction);
        }

        /// <summary>
        /// Finds an IMixer object by its name in the internal collection of mixers
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IMixFunction GetMixFunctionByName(string name)
        {
            var ret = mixFunctions.Find(func => func.Name == name);
            if (ret == null)
                return null;

            return ret;
        }
    }
}
