﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Media;

namespace ColorMixer
{
    /// <summary>
    /// Represents a color mixer that contains color sources and a function to mix them
    /// </summary>
    public sealed class Mixer : IMixer
    {
        readonly List<IMixer> sources = new List<IMixer>();
        Color result;
        Color defaultColor;
        IMixFunction function;

        /// <summary>
        /// Notifies when a property changes its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the actual function mixing the colors.
        /// </summary>
        public IMixFunction Function
        {
            get { return function; }
            set 
            {
                function = value;
                Mix();
                OnPropertyChanged("Function");
            }
        }

        /// <summary>
        /// Gets the minimum amount of data sources needed for calculation. 
        /// </summary>
        public int MinimumSources 
        {
            get
            {
                if (function != null)
                    return function.MinimumSources;
                return int.MaxValue;
            }
        }

        /// <summary>
        /// Gets the name of the color mixer instance. This is for display purposes. Needs to be implemented in the subclass
        /// </summary>
        public string Name
        {
            get
            {
                if(function != null)
                    return function.Name;
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the description of the color mixer type. This is for display purposes. Needs to be implemented in the subclass
        /// </summary>
        public string Description 
        { 
            get
            {
                if (function != null)
                    return function.Description;
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the default visible color that is available in the Result property when the number of color sources in the Sources collection is lower than the value of the MinimumSources property.
        /// </summary>
        /// <remarks>
        /// To update the value of the Result property, the setter calls the Mix() method.
        /// </remarks>
        public Color DefaultColor
        {
            get { return defaultColor; }
            set 
            {
                defaultColor = value;
                Mix();
            }
        }

        /// <summary>
        /// Gets the list of color sources that are used for calculating the Result.
        /// </summary>
        public List<IMixer> Sources
        {
            get { return sources; }
        }

        /// <summary>
        /// Gets the result of the mixing function. This value is valid after the Mix() method is called or after the DefaultColor is set.
        /// </summary>
        public Color Result
        {
            get { return result; }
            private set
            {
                if (!result.Equals(value))
                {
                    result = value;
                    OnPropertyChanged("Result");
                }
            }
        }

        /// <summary>
        /// Adds a color source and recalculates the Result
        /// </summary>
        /// <param name="colorSource">IMixer object providing the source color by the value of its own Result property</param>
        public void AddColorSource(IMixer colorSource)
        {
            if(!sources.Contains(colorSource))
            {
                sources.Add(colorSource);
                colorSource.PropertyChanged += colorSource_PropertyChanged;
                Mix();
            }
        }    
        
        /// <summary>
        /// Removes a color source and bindings, and recalculates the Result
        /// </summary>
        /// <param name="colorSource"></param>
        public void RemoveColorSource(IMixer colorSource)
        {
            if (sources.Contains(colorSource))
            {
                sources.Remove(colorSource);
                colorSource.PropertyChanged -= colorSource_PropertyChanged;
                Mix();
            }
        }

        /// <summary>
        /// Removes all color sources and disconnects from their events
        /// </summary>
        public void RemoveAllSources()
        {
            foreach(var source in sources)
            {
                source.PropertyChanged -= colorSource_PropertyChanged;
            }
            sources.Clear();
            Mix();
        }

        /// <summary>
        /// Runs the mixing function and sets its result value in the Result property
        /// </summary>
        public void Mix()
        {
            if(CanChangeColor())
            {
                Result = defaultColor;
            }
            else
            {
                Result = function.Mix(sources);
            }
        }

        /// <summary>
        /// Defines if the mixer can change color manually.
        /// </summary>
        /// <returns></returns>
        public bool CanChangeColor()
        {
            return sources.Count == 0;
        }

        /// <summary>
        /// Gets the value specifying whether the mixer can accept the specified source.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool CanAcceptSource(IMixer sourceCandidate)
        {          
            return !TraverseSources(this, sourceCandidate);
        }

        private bool TraverseSources(IMixer source, IMixer sourceCandidate)
        {
            if (source == sourceCandidate)
                return true;
            foreach (var child in sourceCandidate.Sources)
            {
                if (TraverseSources(source, child))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Initializes a new instance of this class
        /// </summary>
        public Mixer()
        {
            function = Factory.Instance.DefaultFunction;
        }

        /// <summary>
        /// Raises an event to notify all listeners about a property that changed its value.
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Gets the value specifying whether this object accepts any sources.
        /// </summary>
        /// <returns></returns>
        public bool AcceptsSources()
        {
            if(function != null)
                return function.MinimumSources > 0;
            return false;
        }

       

        void colorSource_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Mix();
        }

    }
}
