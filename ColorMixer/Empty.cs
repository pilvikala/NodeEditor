﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows.Media;

namespace ColorMixer
{
    /// <summary>
    /// Represents an empty mixer that does not accept any sources. This is for usability improvements only, so users 
    /// do not need to think about using some special function as an input node
    /// </summary>
    public sealed class Empty : MixFunctionBase
    {
        /// <summary>
        /// Gets the minimum amount of data sources needed for calculation
        /// </summary>
        public override int MinimumSources
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the name of this Mixer
        /// </summary>
        public override string Name
        {
            get
            {
                return "Input";
            }
        }

        /// <summary>
        /// Gets the descrtiption of this Mixer
        /// </summary>
        public override string Description
        {
            get
            {
                return "Represents a simple color source with no mixing function and no color sources.";
            }
        }

        /// <summary>
        /// Provides default color instead of mixing them.
        /// </summary>
        /// <returns></returns>
        public override Color Mix(List<IMixer> sources)
        {
            Debug.Assert(false); //this function should never get called.
            return Colors.White;
        }
    }
}
