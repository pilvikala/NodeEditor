﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace ColorMixer
{
    public interface IMixFunction:IEquatable<IMixFunction>
    {
        /// <summary>
        /// The minimum amount of sources the function needs to mix the final color. For instance, a negation needs just one, but average works best with 2 or more.
        /// </summary>
        int MinimumSources { get; }

        /// <summary>
        /// Gets the human-readable description of this function
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the name of the mixing function
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Mixes colors from the color sources
        /// </summary>
        /// <param name="sources"></param>
        /// <returns></returns>
        Color Mix(List<IMixer> sources);
    }
}
