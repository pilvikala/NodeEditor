﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;

namespace ColorMixer
{
    public interface IMixer: INotifyPropertyChanged
    {
        /// <summary>
        /// Gets the list of sources providing colors for mixing.
        /// </summary>
        /// <remarks>
        ///  This is for reading purposes only. Add a new source using the AddColorSource(IMixer) method.
        /// </remarks>
        List<IMixer> Sources { get; }
        /// <summary>
        /// Mixes the source colors and calculates the value of the Result
        /// </summary>
        void Mix();

        /// <summary>
        /// Gets or sets the actual function mixing the colors.
        /// </summary>
        IMixFunction Function { get; set; }

        /// <summary>
        /// Gets the Result of the Mix() function
        /// </summary>
        Color Result { get; }
        /// <summary>
        /// Gets the name of the mixer (for display purposes)
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Adds a color source. Adding a color source this way will create all the necessary bindings.
        /// </summary>
        /// <param name="colorSource"></param>
        void AddColorSource(IMixer colorSource);
        /// <summary>
        /// Removes a color source. Removing a color source this way will remove all the necessary bindings.
        /// </summary>
        /// <param name="colorSource"></param>
        void RemoveColorSource(IMixer colorSource);

        /// <summary>
        /// Removes all color sources
        /// </summary>
        void RemoveAllSources();

        /// <summary>
        /// Gets or sets the default color that is used in case of having less sources than the amount specified in the MinimumSources property.
        /// </summary>
        Color DefaultColor { get; set; }
        /// <summary>
        /// Gets or sets the value specifying whether the Mixer can change its color. This is used to determine whether DefaultColor should be used or Result should be calculated by mixing the source colors.
        /// </summary>
        /// <returns>True, when the number of sources is lower than MinimumSources (meaning that the object can change its visible color manually because there is not enough sources to calculate the Result). Otherwise returns False.</returns>
        bool CanChangeColor();

        /// <summary>
        /// Gets or sets the value specifying whether the Mixer accepts any sources. This may affect the view hosting this IMixer object.
        /// </summary>
        bool AcceptsSources();

        /// <summary>
        /// The minimum amount of sources an IMixer calculator needs to mix the final color. For instance, a negation needs just one, but average works best with 2 or more.
        /// </summary>
        /// <remarks>
        /// If the number of sources is lower than this value, the object's Color is equal to DefaultColor.
        /// </remarks>
        int MinimumSources { get; }

        /// <summary>
        /// Gets the description of the mixer
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the value specifying whether the mixer can accept the specified source.
        /// </summary>
        /// <param name="mixer"></param>
        /// <returns></returns>
        bool CanAcceptSource(IMixer mixer);
    }
}
