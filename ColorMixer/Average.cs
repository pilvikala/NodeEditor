﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace ColorMixer
{
    /// <summary>
    /// Mixes source colors into an average color
    /// </summary>
    [Export(typeof(IMixFunction))]
    public class Average:MixFunctionBase
    {
        /// <summary>
        /// Gets the minimum amount of data sources needed for calculation
        /// </summary>
        public override int MinimumSources
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Gets the name of this Mixer
        /// </summary>
        public override string Name
        {
            get
            {
                return "Average";
            }
        }

        /// <summary>
        /// Gets the descrtiption of this Mixer
        /// </summary>
        public override string Description
        {
            get
            {
                return "Calculates the average color from all sources. At least two sources are required for this to have an effect.";
            }
        }
      
        /// <summary>
        /// The actual implementation
        /// </summary>
        /// <returns>Average of all source colors</returns>
        public override Color Mix(List<IMixer> sources)
        {
            if (sources == null)
                throw new ArgumentNullException("sources");

            int r, g, b, i;
            r = g = b = 0;
            for (i = 0; i < sources.Count && i < 255; i++) //this will not mix more than 255 colors. It would not make sense anyway.
            {
                r += sources[i].Result.R;
                g += sources[i].Result.G;
                b += sources[i].Result.B;
            }
            return new Color
            {
                A = Factory.GlobalAlpha,
                R = (byte)(r / i),
                G = (byte)(g / i),
                B = (byte)(b / i)
            };
        }
    }
}
