﻿using ColorMixer;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace Addons
{
    /// <summary>
    /// Mixes source colors using the XOR bitwise function
    /// </summary>
    [Export(typeof(IMixFunction))]
    public class Xor:MixFunctionBase
    {
        /// <summary>
        /// Gets the minimum amount of data sources needed for calculation
        /// </summary>
        public override int MinimumSources
        {
            get
            {
                return 2;
            }
        }

        /// <summary>
        /// Gets the name of this mix function
        /// </summary>
        public override string Name
        {
            get
            {
                return "Xor";
            }
        }

        /// <summary>
        /// Gets the description of this mix function
        /// </summary>
        public override string Description
        {
            get
            {
                return "Performs a bitwise XOR operation on all source color channels. At least two sources are required for this to have an effect.";
            }
        }

        /// <summary>
        /// Mixes the color from its sources
        /// </summary>
        /// <returns>XOR of all source colors</returns>
        /// <remarks>Sources are XOR-ed in the order in which they were added in the collection</remarks>
        public override Color Mix(List<IMixer> sources)
        {
            if (sources == null)
                throw new ArgumentNullException("sources");

            var result = sources[0].Result;
            for (int i = 1; i < sources.Count; i++)
            {
                result.R ^= sources[i].Result.R;
                result.G ^= sources[i].Result.G;
                result.B ^= sources[i].Result.B;
            }
            return result;   
        }
    }
}
