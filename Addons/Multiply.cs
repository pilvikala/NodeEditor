﻿using ColorMixer;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace Addons
{
    /// <summary>
    /// Performs bitwise AND operation on all channels
    /// </summary>
    [Export(typeof(IMixFunction))]
    public class Multiply : MixFunctionBase
    {
        /// <summary>
        /// Gets the minimum amount of data sources needed for calculation
        /// </summary>
        public override int MinimumSources
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Gets the name of this mix function
        /// </summary>
        public override string Name
        {
            get
            {
                return "Multiply";
            }
        }

        /// <summary>
        /// Gets the description of this mix function
        /// </summary>
        public override string Description
        {
            get
            {
                return "Performs a bitwise AND operation on all source color channels. At least two sources are required for this to have an effect.";
            }
        }

        /// <summary>
        /// Mixes the colors from the sources
        /// </summary>
        /// <param name="sources"></param>
        /// <returns></returns>
        public override Color Mix(List<IMixer> sources)
        {
            if (sources == null)
                throw new ArgumentNullException("sources");

            int r, g, b;
            r = sources[0].Result.R;
            g = sources[0].Result.G;
            b = sources[0].Result.B;
            for (int i = 1; i < sources.Count && i < 255; i++) //this will not mix more than 255 colors. It would not make sense anyway.
            {
                r &= sources[i].Result.R;
                g &= sources[i].Result.G;
                b &= sources[i].Result.B;
            }
            return new Color
            {
                A = Factory.GlobalAlpha,
                R = (byte)(r),
                G = (byte)(g),
                B = (byte)(b)
            };
        }
    }
}
