﻿using ColorMixer;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace Addons
{
    /// <summary>
    /// Calculates an inverted color
    /// </summary>
    [Export(typeof(IMixFunction))]
    public class InvertAvg : Average
    {
        /// <summary>
        /// Gets the minimum amount of data sources needed for calculation
        /// </summary>
        public override int MinimumSources
        {
            get
            {
                return 1;
            }
        }
        /// <summary>
        /// Gets the name of this mix function
        /// </summary>
        public override string Name
        {
            get
            {
                return "Invert";
            }
        }

        /// <summary>
        /// Gets the description of this mix function
        /// </summary>
        public override string Description
        {
            get
            {
                return "Inverts the source color. If there are more sources, their average color is calculated first and then inverted.";
            }
        }

        /// <summary>
        /// Mixes the colors from the sources
        /// </summary>
        /// <param name="sources"></param>
        /// <returns></returns>
        public override Color Mix(List<IMixer> sources)
        {
            if (sources == null)
                throw new ArgumentNullException("sources");

            return base.Mix(sources).Invert();
        }
    }
}
