﻿using ColorMixer;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace NodeEditor.Models
{
    /// <summary>
    /// Models a node in a graph
    /// </summary>
    sealed class NodeModel
    {
        private Color color;
        private IMixer mixer;

        /// <summary>
        /// Raised when the color changes
        /// </summary>
        public event EventHandler ColorChanged;
        /// <summary>
        /// Raised when the mixer changes
        /// </summary>
        public event EventHandler MixerChanged;
        /// <summary>
        /// Raised when the mixer disconnects from its sources
        /// </summary>
        public event EventHandler DisconnectedSources;

        /// <summary>
        /// Gets or sets the node color
        /// </summary>
        public Color Color
        {
            get { return color; }
            set 
            {
                if (!color.Equals(value))
                {
                    color = value;
                    OnColorChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the attached IMixer object
        /// </summary>
        public IMixer Mixer
        {
            get { return mixer; }
        }

        /// <summary>
        /// Adds a new color source
        /// </summary>
        /// <param name="source"></param>
        public void AddSource(NodeModel source)
        {
            if (Mixer != null)
            {
                Mixer.AddColorSource(source.Mixer);
            }
        }

        /// <summary>
        /// Removes an existing source
        /// </summary>
        /// <param name="source"></param>
        public void RemoveSource(NodeModel source)
        {
            if(Mixer != null)
            {
                Mixer.RemoveColorSource(source.Mixer);
            }
        }

        /// <summary>
        /// Changes mixing function on this node
        /// </summary>
        /// <param name="function"></param>
        public void ChangeMixFunction(IMixFunction function)
        {            
            if(function.MinimumSources == 0)
            {
                Mixer.RemoveAllSources();
                OnDisconnectedSources();
            }
            Mixer.Function = function;
            OnMixerChanged();
        }

        public NodeModel()
        {
            mixer = new Mixer();
            mixer.PropertyChanged += mixer_PropertyChanged;
        }

        /// <summary>
        /// Specifies whether this node is accepting any sources
        /// </summary>
        /// <returns></returns>
        public bool AcceptsSources()
        {   
            return Mixer.AcceptsSources();
        }

        public bool CanAcceptSource(NodeModel nodeModel)
        {
            return mixer.CanAcceptSource(nodeModel.Mixer);
        }

        void mixer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Result":
                    Color = mixer.Result;
                    break;
                default:
                    break;
            }
        }

        void OnColorChanged()
        {
            var handler = ColorChanged;
            if(handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        void OnMixerChanged()
        {
            var handler = MixerChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        void OnDisconnectedSources()
        {
            var handler = DisconnectedSources;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}
