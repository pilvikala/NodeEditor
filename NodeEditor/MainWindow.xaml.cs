﻿
using ColorMixer;
using NodeEditor.ViewModels;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace NodeEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isEdgeDesignModeEnabled;  //set to true when an edge is being drawn
        NodeViewModel newEdgeSourceNode; //field to hold a temporary reference to a new edge's source node
        bool reverseEdgeDesign; //some people may draw edges the opposite way. reverseEdgeDesign is true when user started drawing an edge from the output connector
        ViewModel viewModel = new ViewModel(); //view model for this window

        /// <summary>
        /// Initializes a new instance of this class
        /// </summary>
        public MainWindow()
        {
            DataContext = viewModel;

            InitializeComponent();
        }

        /// <summary>
        /// Makes the ghost line visible and starts drawing an edge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartEdge(object sender, MouseButtonEventArgs e)
        {
            isEdgeDesignModeEnabled = true;
            newEdgeSourceNode = (NodeViewModel)((Grid)((FrameworkElement)sender).Parent).DataContext;
            ghostLine.Visibility = Visibility.Visible;
            if (reverseEdgeDesign)
            {
                ghostLine.X1 = newEdgeSourceNode.InputConnectorX;
                ghostLine.Y1 = newEdgeSourceNode.InputConnectorY;
                ghostLine.X2 = newEdgeSourceNode.InputConnectorX;
                ghostLine.Y2 = newEdgeSourceNode.InputConnectorY;
            }
            else
            {
                ghostLine.X1 = newEdgeSourceNode.OutputConnectorX;
                ghostLine.Y1 = newEdgeSourceNode.OutputConnectorY;
                ghostLine.X2 = newEdgeSourceNode.OutputConnectorX;
                ghostLine.Y2 = newEdgeSourceNode.OutputConnectorY;
            }
            e.Handled = true;
        }

        /// <summary>
        /// Finishes an edge and creates its model (if not cancelled)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="isCancelled"></param>
        private void FinishEdge(object sender, MouseButtonEventArgs e, bool isCancelled)
        {
            //finish drawing a line
            if (isEdgeDesignModeEnabled && !isCancelled)
            {
                var targetNode = (NodeViewModel)((Grid)((FrameworkElement)sender).Parent).DataContext;                
                if (reverseEdgeDesign)
                {
                    ConnectNodes(targetNode, newEdgeSourceNode);
                }
                else
                {
                    ConnectNodes(newEdgeSourceNode, targetNode);
                }

            }
            isEdgeDesignModeEnabled = false;
            ghostLine.Visibility = Visibility.Collapsed;
            reverseEdgeDesign = false;
            e.Handled = true;
        }

        /// <summary>
        /// Connects two nodes by an edge
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        private void ConnectNodes(NodeViewModel source, NodeViewModel target)
        {
            if (viewModel.ValidateNewConnection(source, target))
            {
                viewModel.Edges.Add(new EdgeViewModel { Source = source, Destination = target });
                target.Model.AddSource(source.Model);   
            }
            else
            {
                MessageBox.Show("You cannot create a circular connection","Sorry!",MessageBoxButton.OK,MessageBoxImage.Stop);
            }
            viewModel.SelectNode(null);
        }

        /// <summary>
        /// Places a new node at a location specified by the supplied event arguments
        /// </summary>
        /// <param name="e"></param>
        private void PlaceNewNode(MouseButtonEventArgs e)
        {
            if (isEdgeDesignModeEnabled)
            {
                //when edge design mode is enabled, click on the grid should cancel it
                isEdgeDesignModeEnabled = false;
                return;//don't create a new node
            }

            var position = e.GetPosition(grid);
            viewModel.PlaceNewNode(position.X, position.Y);
        }

        /// <summary>
        /// Changes the default node color used to color newly created nodes
        /// </summary>
        private void ChangeDefaultColor()
        {
            var window = new ColorpickerWindow();
            ((ColorPickerViewModel)window.DataContext).SelectedColor = viewModel.ActiveColor;
            var result = window.ShowDialog();
            if (result.HasValue && result.Value)
            {
                viewModel.ActiveColor = ((ColorPickerViewModel)window.DataContext).SelectedColor;
            }
        }

        /// <summary>
        /// Changes the default mixer used in the newly created nodes
        /// </summary>
        /// <param name="e"></param>
        private void ChangeDefaultMixer(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;
            viewModel.ActiveMixFunction =  (IMixFunction)e.AddedItems[0];
        }

        /// <summary>
        /// Changes the color of the selected node
        /// </summary>
        private void ChangeSelectedNodeColor()
        {
            var window = new ColorpickerWindow();
            ((ColorPickerViewModel)window.DataContext).SelectedColor = viewModel.SelectedNodeColor;
            var result = window.ShowDialog();
            if (result.HasValue && result.Value)
            {
                viewModel.SelectedNodeColor = ((ColorPickerViewModel)window.DataContext).SelectedColor;
            }
        }

        /// <summary>
        /// Handles dragging of a node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            var thumb = (Thumb)sender;
            var node = (NodeViewModel)thumb.DataContext;
            node.X += e.HorizontalChange;
            node.Y += e.VerticalChange;
        }

        /// <summary>
        /// Finish an edge, cancel selection, or place a new node by left-click on the grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(isEdgeDesignModeEnabled)
            {
                FinishEdge(sender, e, true);
            }
            else if (viewModel.IsAnythingSelected)
            {
                viewModel.SelectNode(null);
            }
            else
            {
                PlaceNewNode(e); 
            }
        }

        /// <summary>
        /// Start or finish drawing a line by clicking an input connector on a node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputConnector_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isEdgeDesignModeEnabled && reverseEdgeDesign)
            {
                FinishEdge(sender, e, false);
            }
            else
            {
                reverseEdgeDesign = false;
                StartEdge(sender, e);
            }
        }

        /// <summary>
        /// Start or finish drawing a line by clicking an output connector on a node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OutputConnector_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isEdgeDesignModeEnabled && !reverseEdgeDesign)
            {
                FinishEdge(sender, e, false);
            }
            else
            {
                reverseEdgeDesign = true;
                StartEdge(sender, e);
            }
        }

        private void ColorSelector_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ChangeDefaultColor();
        }

      

        private void ChangeNodeColor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var nodeViewModel = (NodeViewModel)((FrameworkElement)e.OriginalSource).DataContext;
            var window = new ColorpickerWindow();
            ((ColorPickerViewModel)window.DataContext).SelectedColor = nodeViewModel.Color;
            var result = window.ShowDialog();
            if (result.HasValue && result.Value)
            {
                nodeViewModel.Color = ((ColorPickerViewModel)window.DataContext).SelectedColor;
            }
        }

        private void ChangeNodeColor_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var nodeViewModel = (NodeViewModel)((FrameworkElement)e.OriginalSource).DataContext;
            e.CanExecute = nodeViewModel.CanChangeColor;
        }

        private void ClearAll_Click(object sender, RoutedEventArgs e)
        {
            viewModel.Clear();
        }       

        private void Thumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            var nodeViewModel = (NodeViewModel)((FrameworkElement)e.OriginalSource).DataContext;
            viewModel.SelectNode(nodeViewModel);
        }


        private void DeleteNode_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = viewModel.IsAnythingSelected;
        }

        private void DeleteNode_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            viewModel.DeleteSelectedObjects();
        }

        /// <summary>
        /// this handles the "ghost line" visible when drawing an edge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grid_MouseMove(object sender, MouseEventArgs e)
        {
            if(isEdgeDesignModeEnabled)
            {
                var position = e.GetPosition((UIElement)sender);
                ghostLine.X2 = position.X - 2;
                ghostLine.Y2 = position.Y - 2;
            }
        }

        private void ArrowLine_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var edge = (EdgeViewModel)((FrameworkElement)sender).DataContext;
            viewModel.SelectEdge(edge);
            e.Handled = true;
        }

        private void grid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            viewModel.SelectNode(null);
        }

        private void SelectedNodeColorPicker_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ChangeSelectedNodeColor();
        }

        
    }
}
