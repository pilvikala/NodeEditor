﻿using System.Windows;

namespace NodeEditor
{
    /// <summary>
    /// Interaction logic for ColorpickerWindow.xaml
    /// </summary>
    public partial class ColorpickerWindow : Window
    {
        public ColorpickerWindow()
        {
            Owner = Application.Current.MainWindow;
            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
