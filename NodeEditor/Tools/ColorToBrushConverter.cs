﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace NodeEditor.Tools
{
    /// <summary>
    /// Converts Color to SolidColorBrush
    /// </summary>
    sealed class ColorToBrushConverter:IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var brush = (SolidColorBrush)value;
            return brush.Color;
        }
    }
}
