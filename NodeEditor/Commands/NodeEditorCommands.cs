﻿using System.Windows.Input;

namespace NodeEditor.Commands
{
    /// <summary>
    /// Defines commands specific to Node Editor app.
    /// </summary>
    sealed class NodeEditorCommands
    {
        /// <summary>
        /// Command to change a new node's color (for nodes that will be added by user)
        /// </summary>
        public static RoutedUICommand ChangeNewNodeColor { get; private set; }

        static NodeEditorCommands()
        {
            ChangeNewNodeColor = new RoutedUICommand("C_hange Color", "ChangeNewNodeColor", typeof(NodeEditorCommands));
        }

        
    }
}
