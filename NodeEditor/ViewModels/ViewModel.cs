﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Media;
using ColorMixer;
using System.Diagnostics;
using NodeEditor.Models;
using System.Linq;

namespace NodeEditor.ViewModels
{
    /// <summary>
    /// Provides view model for the main window
    /// </summary>
    sealed class ViewModel : INotifyPropertyChanged
    {
        readonly ObservableCollection<EdgeViewModel> edges = new ObservableCollection<EdgeViewModel>();
        readonly ObservableCollection<NodeViewModel> nodes = new ObservableCollection<NodeViewModel>();
        readonly List<IMixFunction> mixerFunctions = new List<IMixFunction>();
        Color activeColor;
        NodeViewModel selectedNode;
        EdgeViewModel selectedEdge;
        bool hasBeenUsed;

        /// <summary>
        /// Raised when a property changes its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the list of nodes in the graph
        /// </summary>
        public ObservableCollection<NodeViewModel> Nodes
        {
            get { return nodes; }
        }

        /// <summary>
        /// Gets or sets the list of edges in the graph
        /// </summary>
        public ObservableCollection<EdgeViewModel> Edges
        {
            get { return edges; }
        }

        /// <summary>
        /// Specifies the IMixer implementation that is used for newly created nodes.
        /// </summary>
        public IMixFunction ActiveMixFunction { get; set; }

        /// <summary>
        /// Specifies the color that is used for newly created nodes.
        /// </summary>
        public Color ActiveColor
        {
            get { return activeColor; }
            set
            {
                activeColor = value;
                OnPropertyChanged("ActiveColor");
            }
        }

        /// <summary>
        /// Gets the list of functions available for mixing colors
        /// </summary>
        public List<IMixFunction> MixerFunctions
        {
            get 
            {
                return mixerFunctions;
            }
        }

        /// <summary>
        /// gets or sets the mixer (by name) for the selected node
        /// </summary>
        public IMixFunction SelectedNodeMixFunction
        {
            get
            {
                if (selectedNode != null)
                    return selectedNode.Model.Mixer.Function;
                else
                {
                    return null;
                }
            }
            set
            {
                if (selectedNode != null)
                {
                    selectedNode.Model.ChangeMixFunction(value);
                    OnPropertyChanged("SelectedNodeMixFunction");
                    OnPropertyChanged("CanSelectedNodeChangeColor");
                    OnPropertyChanged("SelectedNodeColor");
                }
            }
        }

        /// <summary>
        /// Gets or sets the color of the selected node
        /// </summary>
        public Color SelectedNodeColor
        {
            get
            {
                if (selectedNode != null)
                    return selectedNode.Color;
                else
                {
                    return Colors.Transparent;
                }
            }
            set
            {
                if (selectedNode != null)
                {
                    selectedNode.Color = value;
                    OnPropertyChanged("SelectedNodeColor");
                }
            }
        }

        /// <summary>
        /// Gets the value specifying whether the selected node can change its visible color
        /// </summary>
        public bool CanSelectedNodeChangeColor
        {
            get
            {
                if (selectedNode == null)
                    return false;
                return selectedNode.CanChangeColor;
            }
        }

        /// <summary>
        /// Gets the value specifying whether there is anything selected (highlighted) by the user.
        /// </summary>
        /// <returns></returns>
        public bool IsAnythingSelected
        {
            get
            {
                return selectedEdge != null || IsAnyNodeSelected;
            }
        }

        /// <summary>
        /// Gets the value specifying whether there is any node selected (highlighted) by the user.
        /// </summary>
        public bool IsAnyNodeSelected
        {
            get
            {
                return selectedNode != null;
            }
        }

        /// <summary>
        /// Gets or sets the value specifying whether the application has been used (user added some nodes)
        /// </summary>
        public bool HasBeenUsed
        {
            get { return hasBeenUsed; }
            set 
            {
                hasBeenUsed = value;
                OnPropertyChanged("HasBeenUsed");
            }
        }


        /// <summary>
        /// Initializes a new instance of this class.
        /// </summary>
        public ViewModel()
        {
            ActiveMixFunction = Factory.Instance.DefaultFunction;
            mixerFunctions.AddRange(Factory.Instance.MixFunctions);
            mixerFunctions.Sort((a, b) => a.Name.CompareTo(b.Name));
            ActiveColor = Colors.PaleGoldenrod;
        }

        /// <summary>
        /// Adds a new node
        /// </summary>
        public void PlaceNewNode(double x, double y)
        {
            if (!hasBeenUsed)
            {
                HasBeenUsed = true;
            }
            var node = new NodeViewModel
            {
                X = x,
                Y = y,
            };
            node.DisconnectedSources += node_DisconnectedSources;
            node.Color = ActiveColor;
            Nodes.Add(node);
            node.Model.ChangeMixFunction(ActiveMixFunction);
            
            SelectNode(null);
        }

        /// <summary>
        /// Highlights a node
        /// </summary>
        /// <param name="node"></param>
        public void SelectNode(NodeViewModel node)
        {
            if (selectedNode != null)
                selectedNode.IsSelected = false;

            if (selectedEdge != null)
            {
                selectedEdge.IsSelected = false;
                selectedEdge = null;
            }

            if(node == null)
            {
                selectedNode = null;
            }
            else
            {
                node.IsSelected = true;
                selectedNode = node;
            }
            OnPropertyChanged("IsAnythingSelected");
            OnPropertyChanged("IsAnyNodeSelected");
            OnPropertyChanged("CanSelectedNodeChangeColor");
            OnPropertyChanged("SelectedNodeColor");
            OnPropertyChanged("SelectedNodeMixFunction");
        }

        /// <summary>
        /// Highlights an edge
        /// </summary>
        /// <param name="edge"></param>
        public void SelectEdge(EdgeViewModel edge)
        {
            if (selectedNode != null)
            {
                selectedNode.IsSelected = false;
                selectedNode = null;
            }

            if (selectedEdge != null)
                selectedEdge.IsSelected = false;

            if (edge != null)
            {
                edge.IsSelected = true;
                selectedEdge = edge;
            }
        }

        /// <summary>
        /// Deletes everything
        /// </summary>
        public void Clear()
        {
            Nodes.Clear();
            Edges.Clear();
            selectedNode = null;
        }

        /// <summary>
        /// Deletes selected (highlighted) objects
        /// </summary>
        public void DeleteSelectedObjects()
        {
            if (selectedNode != null)
            {
                var toRemove = new List<EdgeViewModel>();
                foreach(var edge in Edges)
                {
                    if(edge.Destination.Equals(selectedNode) || edge.Source.Equals(selectedNode))
                    {
                        toRemove.Add(edge);
                    }
                }
                toRemove.ForEach(edge => Edges.Remove(edge));
                Nodes.Remove(selectedNode);
                SelectNode(null);
            }
            if(selectedEdge != null)
            {
                Edges.Remove(selectedEdge);
                selectedEdge.Destination.Model.RemoveSource(selectedEdge.Source.Model);
                selectedEdge = null;
            }
        }

        /// <summary>
        /// Validates whether the connection between source and target is possible.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool ValidateNewConnection(NodeViewModel source, NodeViewModel target)
        {
            var edgeExists = null != Edges.FirstOrDefault(edge => edge.Source == source && edge.Destination == target);
            if (edgeExists)
                return false;

            if (source == target)
                return false;

            return target.CanAcceptSource(source);
        }

        /// <summary>
        /// Deletes edges ending in the node's input port because the underlying object just disconnected from its color sources.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void node_DisconnectedSources(object sender, System.EventArgs e)
        {
            var model = (NodeViewModel)sender;
            var remove = new List<EdgeViewModel>();
            foreach(var edge in edges)
            {
                if(edge.Destination == model)
                {
                    remove.Add(edge);
                }
            }
            remove.ForEach(edge => edges.Remove(edge));
        }

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
