﻿using ColorMixer;
using System.ComponentModel;
using System.Windows.Media;

namespace NodeEditor.ViewModels
{
    /// <summary>
    /// View model for the ColorPicker window
    /// </summary>
    sealed class ColorPickerViewModel : INotifyPropertyChanged
    {
        private Color selectedColor;
        private byte r;
        private byte g;
        private byte b;

        /// <summary>
        /// Raised when a property changes its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the value of the red channel of the selected color
        /// </summary>
        public byte R
        {
            get { return r; }
            set
            {
                if (r != value)
                {
                    r = value;
                    OnPropertyChanged("R");
                    SelectedColor = GetColor(r, g, b);
                }
            }
        }

        /// <summary>
        /// Gets or sets the value of the green channel of the selected color
        /// </summary>
        public byte G
        {
            get { return g; }
            set
            {
                if (g != value)
                {
                    g = value;
                    OnPropertyChanged("G");
                    SelectedColor = GetColor(r, g, b);
                }
            }
        }

        /// <summary>
        /// Gets or sets the value of the blue channel of the selected color
        /// </summary>
        public byte B
        {
            get { return b; }
            set
            {
                if (b != value)
                {
                    b = value;
                    OnPropertyChanged("B");
                    SelectedColor = GetColor(r, g, b);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected color
        /// </summary>
        public Color SelectedColor
        {
            get { return selectedColor; }
            set
            {
                if (!selectedColor.Equals(value))
                {
                    selectedColor = value;
                    OnPropertyChanged("SelectedColor");
                    r = selectedColor.R;
                    g = selectedColor.G;
                    b = selectedColor.B;
                    OnPropertyChanged("R");
                    OnPropertyChanged("G");
                    OnPropertyChanged("B");
                    OnPropertyChanged("PreviewTextColor");
                }
            }
        }

        /// <summary>
        /// Gets the inverted selected color that is used for the text on nodes
        /// </summary>
        public Color PreviewTextColor
        {
            get
            {
                return selectedColor.GetContrastColor();
            }
        }

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        static Color GetColor(byte r, byte g, byte b)
        {
            return new Color() { A = 255, B = b, G = g, R = r };
        }
    }
}
