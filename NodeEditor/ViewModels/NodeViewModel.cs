﻿using ColorMixer;
using System;
using System.ComponentModel;
using System.Windows.Media;

namespace NodeEditor.ViewModels
{
    sealed class NodeViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets the default node height
        /// </summary>
        public static double DefaultHeight { get; private set; }
        
        /// <summary>
        /// Gets the default node width
        /// </summary>
        public static double DefaultWidth { get; private set; }

        private static int id;
        /// <summary>
        /// this is for generating the default node name (text)
        /// </summary>
        private static int Id
        {
            get { return id++; }
            set { id = value; }
        }

        string text;
        double x;
        double y;
        double width;
        double height;
        Color color;
        double connectorSize;
        double connectorAreaWidth;
        double inputConnectorX;
        Models.NodeModel model;
        double inputConnectorY;
        double outputConnectorX;
        double outputConnectorY;
        bool isSelected;

        /// <summary>
        /// Raised when a property changes its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raised when the uderlying model disconnects from all sources (as a result of changing its mix function)
        /// </summary>
        public event EventHandler DisconnectedSources;

        /// <summary>
        /// Gets the node model holding the data about this node. Kind of. The line between view model and model is always a bit fuzzy.
        /// </summary>
        public Models.NodeModel Model
        {
            get { return model; }
        }

        /// <summary>
        /// Gets or sets the x position of the input connector (where the source edges connect to).
        /// </summary>
        public double InputConnectorX
        {
            get { return inputConnectorX; }
            set
            {
                inputConnectorX = value;
                OnPropertyChanged("InputConnectorX");
            }
        }

        /// <summary>
        /// Gets or sets the y position of the input connector (where the source edges connect to).
        /// </summary>
        public double InputConnectorY
        {
            get { return inputConnectorY; }
            set
            {
                inputConnectorY = value;
                OnPropertyChanged("InputConnectorY");
            }
        }

        /// <summary>
        /// Gets or sets the x position of the output connector (where the output edges start at).
        /// </summary>
        public double OutputConnectorX
        {
            get { return outputConnectorX; }
            set
            {
                outputConnectorX = value;
                OnPropertyChanged("OutputConnectorX");
            }
        }

        /// <summary>
        /// Gets or sets the y position of the output connector (where the output edges start at).
        /// </summary>
        public double OutputConnectorY
        {
            get { return outputConnectorY; }
            set
            {
                outputConnectorY = value;
                OnPropertyChanged("OutputConnectorY");
            }
        }

        /// <summary>
        /// Gets or sets the connector size. Connector is the elipse on the left and right side of the node's rectangle
        /// </summary>
        public double ConnectorSize
        {
            get { return connectorSize; }
            set
            {
                connectorSize = value;
                OnPropertyChanged("ConnectorSize");
            }
        }

        /// <summary>
        /// Gets or sets the width of the connector area
        /// </summary>
        public double ConnectorAreaWidth
        {
            get { return connectorAreaWidth; }
            set
            {
                connectorAreaWidth = value;
                OnPropertyChanged("connectorAreaWidth");
            }
        }

        /// <summary>
        /// Gets or sets the text displayed on the node
        /// </summary>
        public string Text
        {
            get 
            {
                return text;
            }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }

        /// <summary>
        /// Gets or sets the node's visible color
        /// </summary>
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                if (color == value)
                {
                    return;
                }

                
                if(model != null)
                {
                    if (CanChangeColor)
                    {
                        model.Mixer.DefaultColor = value;
                    }
                    color = model.Mixer.Result;
                }
                else
                {
                    color = value;
                }
                OnPropertyChanged("Color");
                OnPropertyChanged("TextColor");
            }
        }

        /// <summary>
        /// Gets the human-readable name of the current color
        /// </summary>
        public string ColorName
        {
            get { return color.GetName(); }
        }


        /// <summary>
        /// Gets or sets the horizontal position of the node
        /// </summary>
        public double X
        {
            get { return x; }
            set
            {
                x = value;
                OnPropertyChanged("X");
                InputConnectorX = x;
                OutputConnectorX = x + width;
            }
        }

        /// <summary>
        /// Gets or sets the vertical position of the node
        /// </summary>
        public double Y
        {
            get { return y; }
            set
            {
                y = value;
                OnPropertyChanged("Y");
                OutputConnectorY = InputConnectorY = y + height / 2;
            }
        }

        /// <summary>
        /// Gets or sets the width of the node
        /// </summary>
        public double Width
        {
            get { return width; }
            set
            {
                width = value;
                ConnectorAreaWidth = 0.15 * width;
                ConnectorSize = connectorAreaWidth - 2;
                OnPropertyChanged("Width");
                InputConnectorX = x;
                OutputConnectorX = x + width;
            }
        }

        /// <summary>
        /// Gets or sets the height of the node
        /// </summary>
        public double Height
        {
            get { return height; }
            set
            {
                height = value;
                OnPropertyChanged("Height");
                OutputConnectorY = InputConnectorY = y + height / 2;
            }
        }

        /// <summary>
        /// Gets the value specifying whether the user is allowed to change the color of this node manually. 
        /// When there are sources attached to the node, it is not desirable to change its color
       ///  because it should be calculated automatically by the underlying IMixer object.
        /// </summary>
        public bool CanChangeColor
        {
            get
            {
                if (Model == null || Model.Mixer == null)
                    return false;

                return Model.Mixer.Sources.Count == 0;
            }
        }

        /// <summary>
        /// Gets or sets the value specifying whether the node has been selected (highlighted) by the user
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }

            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets the color of the node's text. 
        /// </summary>
        /// <remarks>Since this color basically depends on the actual Color property, change of the Color property notifies about the change of this property too.</remarks>
        public Color TextColor
        {
            get
            {
                return color.GetContrastColor();
            }
        }

        /// <summary>
        /// Gets the value specifying whether this node can accept any sources.
        /// </summary>
        public bool AcceptsSources
        {
            get
            {
                return model.AcceptsSources();
            }
        }

        static NodeViewModel()
        {
            DefaultHeight = 50;
            DefaultWidth = 100;
        }

        /// <summary>
        /// Initializes a new instance of this class and creates the internal NodeModel.
        /// </summary>
        public NodeViewModel()
        {
            Text = "Node " + Id;
            Width = DefaultWidth;
            Height = DefaultHeight;
            model = new Models.NodeModel();
            model.ColorChanged += model_ColorChanged;
            model.MixerChanged += model_MixerChanged;
            model.DisconnectedSources += model_DisconnectedSources;
        }

        public bool CanAcceptSource(NodeViewModel source)
        {
            return model.CanAcceptSource(source.Model);
        }

        void model_DisconnectedSources(object sender, EventArgs e)
        {
            OnDisconnectedSources();
        }

        void model_MixerChanged(object sender, EventArgs e)
        {
            if (model.Mixer != null)
            {
                Text = model.Mixer.Name;
            }
            OnPropertyChanged("AcceptsSources");
            OnPropertyChanged("Color");
        }

        void model_ColorChanged(object sender, EventArgs e)
        {
            Color = model.Color;
        }

        /// <summary>
        /// Raises the PropertyChanged event.
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Raises the PropertyChanged event.
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnDisconnectedSources()
        {
            var handler = DisconnectedSources;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

    }
}
