﻿using System.ComponentModel;

namespace NodeEditor.ViewModels
{
    /// <summary>
    /// View model for an edge in the graph
    /// </summary>
    sealed class EdgeViewModel : INotifyPropertyChanged
    {
        bool isSelected;
        NodeViewModel source;
        NodeViewModel destination;

        /// <summary>
        /// Raised when a property changes its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the source node of the edge - the node where the edge starts.
        /// </summary>
        public NodeViewModel Source
        {
            get { return source; }
            set
            {
                source = value;
                OnPropertyChanged("Source");
            }
        }

        /// <summary>
        /// Gets or sets the destination node of the edge - the node where the edge ends.
        /// </summary>
        public NodeViewModel Destination
        {
            get { return destination; }
            set
            {
                destination = value;
                OnPropertyChanged("Destination");
            }
        }

        /// <summary>
        /// Gets or sets the value specifying whether the edge is selected (highlighted by user).
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }

            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
