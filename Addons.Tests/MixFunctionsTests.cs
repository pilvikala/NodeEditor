﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ColorMixer;
using System.Windows.Media;
using System.Collections.Generic;

namespace Addons.Tests
{
    [TestClass]
    public class MixFunctionsTests
    {
        IMixer red, lime, blue;

        [TestInitialize]
        public void Setup()
        {
            red = new Mixer() { DefaultColor = Colors.Red, Function = Factory.Instance.DefaultFunction };
            lime = new Mixer() { DefaultColor = Colors.Lime, Function = Factory.Instance.DefaultFunction };
            blue = new Mixer() { DefaultColor = Colors.Blue, Function = Factory.Instance.DefaultFunction };
        }

        [TestMethod]
        public void TestAdditive()
        {
            var func = new Additive();
            var actual = func.Mix(new List<IMixer> { red, lime, blue });
            Assert.AreEqual(Colors.White, actual);
        }

        [TestMethod]
        public void TestInvertAverage()
        {
            var func = new InvertAvg();
            var actual = func.Mix(new List<IMixer> { red, lime, blue });
            var expected = new Color()
            {
                A = Factory.GlobalAlpha,
                R = 0xaa,
                G = 0xaa,
                B = 0xaa
            };
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMultiply()
        {
            var func = new Multiply();
            var actual = func.Mix(new List<IMixer> { red, lime, blue });
            Assert.AreEqual(Colors.Black, actual);
        }

        [TestMethod]
        public void TestXor()
        {
            var func = new Xor();
            var actual = func.Mix(new List<IMixer> { red, lime, blue });
            Assert.AreEqual(Colors.White, actual);
        }
    }
}
