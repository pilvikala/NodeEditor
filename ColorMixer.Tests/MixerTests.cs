﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ColorMixer.Tests
{
    [TestClass]
    public class MixerTests
    {
        [TestMethod]
        public void CanAcceptSourceTest()
        {
            var mix1 = new Mixer{Function = Factory.Instance.GetMixFunctionByName("Average")};
            var mix2 = new Mixer{Function = Factory.Instance.GetMixFunctionByName("Average")};
            var mix3 = new Mixer{Function = Factory.Instance.GetMixFunctionByName("Average")};
            
            mix2.AddColorSource(mix1);
            mix3.AddColorSource(mix2);
            
            var actual = mix1.CanAcceptSource(mix3);
            Assert.IsFalse(actual);
            actual = mix3.CanAcceptSource(mix1);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CanAcceptSourceTest2()
        {
            var mix1 = new Mixer { Function = Factory.Instance.GetMixFunctionByName("Average") };
            var mix2 = new Mixer { Function = Factory.Instance.GetMixFunctionByName("Average") };
            var mix3 = new Mixer { Function = Factory.Instance.GetMixFunctionByName("Average") };
            var mix4 = new Mixer { Function = Factory.Instance.GetMixFunctionByName("Average") };
            var mix5 = new Mixer { Function = Factory.Instance.GetMixFunctionByName("Average") };

            Assert.IsTrue(mix2.CanAcceptSource(mix1));
            mix2.AddColorSource(mix1);
            Assert.IsTrue(mix3.CanAcceptSource(mix2));
            mix3.AddColorSource(mix2);
            Assert.IsTrue(mix4.CanAcceptSource(mix1));
            mix4.AddColorSource(mix1);
            Assert.IsTrue(mix3.CanAcceptSource(mix4));
            mix3.AddColorSource(mix4);

            var actual = mix5.CanAcceptSource(mix3);
            Assert.IsTrue(actual);
            
        }
    }
}
