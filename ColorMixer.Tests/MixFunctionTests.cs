﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Media;
using System.Collections.Generic;

namespace ColorMixer.Tests
{
    [TestClass]
    public class MixFunctionTests
    {
        IMixer red,lime,blue;

        [TestInitialize]
        public void Setup()
        {
            red = new Mixer() { DefaultColor = Colors.Red, Function = Factory.Instance.DefaultFunction };
            lime = new Mixer() { DefaultColor = Colors.Lime, Function = Factory.Instance.DefaultFunction };
            blue = new Mixer() { DefaultColor = Colors.Blue, Function = Factory.Instance.DefaultFunction };
        }

        [TestMethod]
        public void TestAverage()
        {
            var average = new Average();
            var actual = average.Mix(new List<IMixer> { red,lime,blue});
            var expected = new Color()
            {
                A = Factory.GlobalAlpha,
                R = 0x55,
                G = 0x55,
                B = 0x55
            };
            Assert.AreEqual(expected, actual);
        }
        
        /// <summary>
        /// test chaining red+lime+blue=>average=>average2
        /// </summary>
        [TestMethod]
        public void TestChaining()
        {
            var average = new Mixer()
            {
                Function = Factory.Instance.GetMixFunctionByName("Average")
            };
            
            var average2 = new Mixer()
            {
                Function = Factory.Instance.GetMixFunctionByName("Average")
            };
            average2.AddColorSource(average);

            average.AddColorSource(red);
            Assert.AreEqual(Colors.Red, average.Result);
            Assert.AreEqual(Colors.Red, average2.Result);
            average.AddColorSource(blue);
            average.AddColorSource(lime);
            var expected = new Color()
            {
                A = Factory.GlobalAlpha,
                R = 0x55,
                G = 0x55,
                B = 0x55
            };
            Assert.AreEqual(expected, average.Result);
            Assert.AreEqual(expected, average2.Result);

            average.RemoveColorSource(red);
            
            expected = new Color()
            {
                A = Factory.GlobalAlpha,
                R = 0,
                G = 0x7f,
                B = 0x7f
            };

            Assert.AreEqual(expected, average.Result);
            Assert.AreEqual(expected, average2.Result);
        }

        /// <summary>
        /// Tests the effect of the DefaultColor property
        /// </summary>
        [TestMethod]
        public void TestDefaultColor()
        {
            var average = new Mixer()
            {
                Function = Factory.Instance.GetMixFunctionByName("Average")
            };
            average.DefaultColor = Colors.Purple;
            average.AddColorSource(red);
            Assert.AreEqual(Colors.Red, average.Result);
            
            average.RemoveColorSource(red);

            Assert.AreEqual(Colors.Purple, average.Result);
            
        }
    }


}
