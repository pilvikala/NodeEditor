﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Media;

namespace ColorMixer.Tests
{
    [TestClass]
    public class ExtensionsTests
    {
        [TestMethod]
        public void TestInvert()
        {
            var color = Colors.Blue;
            Assert.AreEqual(Colors.Yellow, color.Invert());
        }

        [TestMethod]
        public void TestGetColorName()
        {
            var color = Colors.Blue;
            Assert.AreEqual("Blue", color.GetName());

            color = new Color()
            {
                A = 255,
                R = 0x2,
                G = 0x2,
                B = 0x2
            };
            Assert.AreEqual("#FF020202", color.GetName());
        }

    }
}
